import requests

from src.servers.streamango import Streamango


class Series:

    def get_series(self):
        self.is_not_used()
        return requests.get('http://173.255.196.18:2828/series/').json()

    def is_alive(self, url):
        self.is_not_used()
        if 'rapidvideo' in url:
            if requests.get(url).status_code == 200:
                return True
        if 'streamango' in url:
            return Streamango().is_alive(url)
        return False

    def update_data_serie(self, url, chapter):
        self.is_not_used()
        server = self.return_server(url)
        if server == 'rapidvideo':
            chapter['key'] = url
            chapter['rapidvideo'] = True
            chapter['openload'] = False
            chapter['streamango'] = False
        elif server == 'streamango':
            chapter['key'] = url
            chapter['rapidvideo'] = False
            chapter['openload'] = False
            chapter['streamango'] = True
        else:
            return {
                'status': 'Invalid link',
                'sample': {
                    'rapidvideo': 'https://www.rapidvideo.com/v/EFSFD23F',
                    'streamango': 'https://streamango.com/f/ftsalroeckklsakb'
                }
            }
        return chapter

    def update_serie(self, serie):
        self.is_not_used()
        return requests.put('http://173.255.196.18:2828/movie/', data=serie).json()

    def return_server(self, url):
        self.is_not_used()
        if 'https://www.rapidvideo.com/v/' in url:
            return 'rapidvideo'
        elif 'https://streamango.com/f/' in url:
            return 'streamango'

    def is_not_used(self):
        pass
