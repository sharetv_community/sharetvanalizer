import urllib
from urllib.request import urlopen

from bs4 import BeautifulSoup


class AppURLopener(urllib.request.FancyURLopener):
    version = "Mozilla/5.0"


class Streamango:

    def is_alive(self, url):
        self.is_not_used()
        opener = AppURLopener()
        html = opener.open(url)
        res = BeautifulSoup(html.read(), "html5lib")
        if 'File not found' in res.title:
            return False
        return True

    def is_not_used(self):
        pass
