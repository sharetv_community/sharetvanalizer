from distlib.compat import raw_input

from src.movies.movies import Movies
from src.series.series import Series


def check_movies():
    movies = Movies()
    data = movies.get_movies()
    for movie in data:
        if movies.is_alive(movie['key']):
            print('Movie %s is alive' % movie['title'])
        else:
            new_url = raw_input("Please enter new url for %s: " % movie['title'])
            new_movie = movies.update_data_movie(new_url, movie)
            response = movies.update_movie(new_movie)
            print(response)


def check_series():
    series = Series()
    data = series.get_series()
    for serie in data:
        for season in serie['seasons']:
            chapter_position = 0
            for chapter in season['season']['chapters']:
                if series.is_alive(chapter['key']):
                    print('Serie %s: chapter %s is alive' % (serie['title'], chapter_position + 1))
                else:
                    new_url = raw_input("Please enter new url for %s: " % serie['title'])
                    new_chapter = series.update_data_serie(new_url, chapter)
                    response = series.update_serie(new_chapter)
                    season['season']['chapters'][chapter_position] = response
                    print(response)
                chapter_position += 1


check_movies()
check_series()
